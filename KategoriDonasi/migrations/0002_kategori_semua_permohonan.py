# Generated by Django 2.2.6 on 2019-10-15 16:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PermohonanDonasi', '0013_permohonan_kategori_alasan'),
        ('KategoriDonasi', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='kategori',
            name='semua_permohonan',
            field=models.ManyToManyField(to='PermohonanDonasi.Permohonan'),
        ),
    ]
