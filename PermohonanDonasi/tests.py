from django.test import TestCase
from .models import Permohonan
import django.utils.timezone

# Create your tests here.
class PermohonanDonasiTest(TestCase):
    # def test_fail(self):
    #     self.fail("fail oiiii")
    
    def test_object_to_string(self):
        permohonan = Permohonan(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",
                                bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",
                                target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan"
                                )
        self.assertEqual(str(permohonan), permohonan.nama_donasi)

    def test_default_image(self):
        permohonan = Permohonan(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",
                                bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",
                                target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan"
                                )
        self.assertEqual(permohonan.image_donasi, "images/donatsi.png")

    def test_default_tanggal_pembuatan(self):
        permohonan = Permohonan(nama_pemohon="Kefas Satrio", email="kefassatrio@gmail.com",
                                bank="BCA", rekening=12345678910, nama_donasi="beliin telur dong mau bikin donat",
                                target_uang=2500000, uang_terpenuhi=0, deadline="2020-12-12", deskripsi_alasan="mau makan"
                                )
        self.assertEqual(permohonan.tanggal_pembuatan, django.utils.timezone.now())

    