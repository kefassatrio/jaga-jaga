from django.shortcuts import render, redirect
from .forms import CreatePermohonan
from KategoriDonasi.models import Kategori
from .models import Permohonan

# Create your views here.
def daftar_permohonan(request):
    form = CreatePermohonan()
    if request.method == "POST":
        form = CreatePermohonan(request.POST, request.FILES)
        if form.is_valid():
            permohonan = form.save(commit=False)
            permohonan.urgency = permohonan.kategori_alasan.urgency    
            permohonan.save()
            return redirect("homepage")
    return render(request, "daftar-permohonan.html", {'form': form})

def list_permohonan(request):
    query = request.GET.get("query")
    if query:
        objects = Permohonan.objects.filter(nama_donasi__contains=query)
    else:
        objects = Permohonan.objects.all()
    return render(request, 'semua-permohonan.html', {"objects": objects})

def detail_permohonan(request, id):
    permohonan = Permohonan.objects.get(id=id)
    return render(request, 'detail-permohonan.html', {"permohonan": permohonan})