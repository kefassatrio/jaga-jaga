# Generated by Django 2.2.6 on 2019-10-13 05:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PermohonanDonasi', '0002_auto_20191013_1159'),
    ]

    operations = [
        migrations.AddField(
            model_name='permohonan',
            name='image_donasi',
            field=models.ImageField(default='images/donatsi.png', upload_to='images'),
        ),
    ]
