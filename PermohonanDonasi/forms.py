from django import forms
from . import models
from django.utils import timezone
from datetime import datetime

class DateInput(forms.DateInput):
    input_type = "date"


class CreatePermohonan(forms.ModelForm):
    class Meta:
        model = models.Permohonan
        fields = "__all__"
        exclude = ['uang_terpenuhi', 'urgency', 'comments', 'semua_pemberian', 'tanggal_pembuatan']

        widgets = {
            "deadline": DateInput
        }

# class SearchPermohonan(forms.Form):
#     searched = forms.CharField(max_length=100)