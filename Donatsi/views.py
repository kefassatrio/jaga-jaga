from django.shortcuts import render, redirect
from PermohonanDonasi.models import Permohonan
# from PermohonanDonasi.forms import SearchPermohonan

# Create your views here.
def homepage(request):
    recommendations = []
    for i in range(5,0,-1):
        permohonan_query = Permohonan.objects.filter(urgency = i)
        for permohonan in permohonan_query:
            if(len(recommendations) < 6):
                recommendations.append(permohonan)
            else:
                break
        if(len(recommendations) >= 6):
            break

    return render(request,"homepage.html", {"recommendations": recommendations})

def about(request):
    return render(request, "about.html")