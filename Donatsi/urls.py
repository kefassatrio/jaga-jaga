from django.contrib import admin
from django.urls import path, include

from django.conf.urls.static import  static
from . import settings
from .views import homepage, about

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', homepage, name="homepage"), #homepage ntar
    path('tentang-kami/', about, name="about"),
    path('donasi/', include("PermohonanDonasi.urls", namespace="permohonan")),
    path('donasi/', include("PemberianDonasi.urls", namespace="pemberian")),
]


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)